package com.pelikan;
/**
 * This is our class for operations with IntervalNumbers , here we will write our code for that.
 *
 * @version 1.01 27 Aug 2019
 *
 * @author Bogdan Pelikan
 */
final class IntervalOfNumber {
  /** this is private constructor because our class is Utility. */
  private IntervalOfNumber() {
    throw new IllegalStateException("Utility class");
  }
  /** here we collect sum of odd numbers from our interval. */
  private static int oddSum = 0;
  /** here we collect sum of even numbers from our interval. */
  private static int evenSum = 0;

  /**
   * In this method we define our interval with two numbers and printing odds numbers in ascending order.
   * then even numbers in descending order
   *
   * @param start Here we define our first number(start point) for interval
   *
   * @param end Here we define our second number(end point) for interval
   */
  static void getIntervalNumbers(final int start, final int end) {
    System.out.println("Odds numbers from interval in ascending order are : ");
    for (int i = start; i <= end; i++) {
      if (i % 2 != 0) {
        oddSum += i;
        System.out.print(i + " ");
      }
    }

    System.out.println("\nEven numbers from interval in descending order are : ");
    for (int i = end; i >= start; i--) {
      if (i % 2 == 0) {
        evenSum += i;
        System.out.print(i + " ");
      }
    }
  }

  /** In this method we print sum of even and odd numbers. */
  static void getEvenAndOddSum() {
    System.out.println(
        "\nSum of even numbers is : " + " " + evenSum + " " + "and for odd numbers is : " + oddSum);
  }
}
