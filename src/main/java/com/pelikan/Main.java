package com.pelikan;

import java.util.Scanner;

/**
 * This is our main сlass , here we will write our code.
 *
 * @version 1.01 27 Aug 2019
 * @author Bogdan Pelikan
 */
public class Main {
  /**
   * Here is start point of our program,where we will call methods from another classes.
   * First you need to print in console three integer numbers : first and second one is first and last number
   * for our interval then you need to set size how much Fibonacci numbers you would like to
   * generate After defining all what we need you can start calling static methods from other
   * classes to do some operations For that purpose just type class name and after that dot
   *
   * @param args command line values
   */
  public static void main(final String[] args) {
    System.out.println(
        "Please,print first number of interval and last one and then set size of Fibonacci numbers");
    Scanner scan = new Scanner(System.in);
    int numA = scan.nextInt();
    int numB = scan.nextInt();
    int size = scan.nextInt();
    IntervalOfNumber.getIntervalNumbers(numA, numB);
    IntervalOfNumber.getEvenAndOddSum();
    Fibonacci.calcFibonacci(size);
    Fibonacci.getOddAndEvenPrecantage(size);
  }
}
