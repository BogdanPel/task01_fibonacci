package com.pelikan;
/**
 * This is our class for Fibonacci numbers operations , here we will write our code for that.
 *
 * @version 1.01 27 Aug 2019
 *
 * @author Bogdan Pelikan
 */
final class Fibonacci {
  /** Its constant to set percentage value what we want. */
  private static final int PERCENTAGE_VALUE = 100;
  /** Its constant what we use in our loop as a start parameter. */
  private static final int FIRST_THREE_FIBONACCI_VALUES = 3;
  /** this is private constructor because our class is Utility. */
  private Fibonacci() {
    throw new IllegalStateException("Utility class");
  }
  /** this our variable for collecting sum of Fibonacci even numbers. */
  private static int fibonacciEvenSum = 0;

  /**
   * This method calculates and generate Fibonacci numbers for already specified size in our param.
   *
   * @param size here we specify how many numbers of Fibonacci we wanna generate
   */
  static void calcFibonacci(final int size) {
    System.out.println("Printing" + " " + size + " " + "Fibonacci numbers");
    int number0 = 1;
    int number1 = 1;
    int number2;
    System.out.print(number0 + " " + number1 + " ");
    for (int i = FIRST_THREE_FIBONACCI_VALUES; i <= size; i++) {
      number2 = number0 + number1;
      System.out.print(number2 + " ");
      number0 = number1;
      number1 = number2;
      if (number0 % 2 == 0 || number1 % 2 == 0) {
        fibonacciEvenSum++;
      }
    }
    System.out.println();
  }

  /**
   * This method calculates precantage of even and odd numbers in our Fibonacci.
   *
   * @param size here we need to specify size of all numbers
   */
  static void getOddAndEvenPrecantage(final int size) {
    int evenPercentage = fibonacciEvenSum * PERCENTAGE_VALUE / size;
    int oddPercentage = PERCENTAGE_VALUE - evenPercentage;
    System.out.println("Even Numbers precantage is : " + " " + evenPercentage);
    System.out.println("Odd Numbers precantage is : " + " " + oddPercentage);
  }
}
